package spil;

import java.io.Console;


/**
 * @author txx
 * 
 * Hoved og start klasse p� spillet
 */
public class Spil {
	
	public static final String title = " S R S P ";
	public static final String version = "0.1.1";
	
	public static final boolean debug = true;
	public static Debug dbg;
	
	public static void main(String[] args) {
		if(debug) { dbg = new Debug(); }
		Spil mitSpil = new Spil();
	}
	
	public Spil() {
		if(debug) { dbg.p("Loading game", 0); }
		Vindue spilvindue = new Vindue(this);
	}

}

class Debug {
	
	private static String tag = "[DEBUG]-";
	private static String[] phase = {"Initializing", "Menu", "Scene", "Running", "Crash", "Warning", "Error"};
	
	public Debug() {
		p("Loaded debug", 0);
	}
	
	public void p(String msg, int pID) {
		System.out.println(tag + phase[pID] + ": " + msg + ".");
	}
	
}