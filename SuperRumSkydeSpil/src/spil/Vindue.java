package spil;


import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author txx
 *
 * Vindue til at holde spillets grafik
 */
public class Vindue extends Application {
	
	protected Spil mitSpil;
	
	public Vindue(Spil spillet) {
		mitSpil = spillet;
		if(mitSpil.debug) { mitSpil.dbg.p("Loading game window", 0); }

		
		//launch(); // Add args?
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle(this.mitSpil.title + " - " + this.mitSpil.version);
		
		
		primaryStage.show();
	}

}
