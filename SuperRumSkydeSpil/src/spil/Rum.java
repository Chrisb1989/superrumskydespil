package spil;

import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.animation.Timeline;

/** 
 * @author txx
 * 
 * rummet som hele spillet bliver spillet i
 */
public class Rum {
	
	private static int BIS = 60; //Billeder i sekundet.
	private Timeline spilSløjfe;
	private Scene spilBræt;
	private Group brætStykker;
	private FeBestyrer feBestyrer;
	
	public Rum() {
		
	}
	
	public void startSpilSløjfe() {
		
	}
	
	public void fåSpilBræt() {
		
	}
	
	public void fåBrætStykker() {
		
	}
	
	protected void bygSpilSløjfe() {
		
	}
	
	protected void opdaterFeer() {
		
	}
	
	protected void håndterOpdatering(Fe fe) {
		
	}
	
	protected void undersøgUheld() {
		
	}
	
	protected boolean håndterUheld(Fe fe1, Fe fe2) {
		return false;
	}
	
	protected void rengørFe() {
		
	}
	
	protected int fåBIS() {
		return this.BIS;
	}
	
	protected Timeline fåSpilSløjfe() {
		return this.spilSløjfe;
	}
	
	protected void sætSpilSløjfe(Timeline nySløjfe) {
		this.spilSløjfe = nySløjfe;
	}
	
	protected FeBestyrer fåFeBestyrer() {
		return this.feBestyrer;
	}

	protected void sætSpilBræt(Scene nytSpilBræt) {
		this.spilBræt = nytSpilBræt;
	}
	
	protected void sætBrætStykker(Group nyeBrætStykker) {
		this.brætStykker = nyeBrætStykker;
	}
}