package spil;

import java.util.List;
import java.util.Set;

/**
 * @author txx
 *	FeBestyreren holder styr på alle feerene i spillet.
 */
public class FeBestyrer {
	
	private List<Fe> skuespillere;
	private List<Fe> feUheld;
	private Set<Fe>	feSkrald;
	
	public FeBestyrer() {

	}
	
	public void tilføjFeer(Fe[] feer) {
		
	}
	
	public void fjernFeer(Fe[] feer) {
		
	}
	
	public Set<Fe> fåFeSkrald() {
		return null;
	}
	
	public void tilføjFeSkrald(Fe[] feer) {
		
	}
	
	public List<Fe> fåFeUheld() {
		return null;
	}
	
	public void nulstilFeUheld() {
		
	}
	
	public void rengørFeer() {
		
	}
	
}
